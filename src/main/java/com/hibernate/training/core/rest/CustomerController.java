package com.hibernate.training.core.rest;

import com.hibernate.training.core.EntityGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/create")
public class CustomerController {

    private EntityGenerator entityGenerator;

    @Autowired
    public CustomerController(EntityGenerator entityGenerator) {
        this.entityGenerator = entityGenerator;
    }

    @PostMapping(path = "/customer")
    public void createCustomerData() {
        entityGenerator.generateData();
    }
}
