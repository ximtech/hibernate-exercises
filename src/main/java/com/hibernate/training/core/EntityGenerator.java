package com.hibernate.training.core;

import com.hibernate.training.core.domain.CustomerUser;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@Service
public class EntityGenerator {  // TEST COMMIT

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public void generateData() {
        CustomerUser customerUser = new CustomerUser();
        customerUser.setAccountLocked(false);
        customerUser.setEnabled(true);
        customerUser.setDeleted(false);
        customerUser.setFirstName("CompanyUserName");
        customerUser.setLastName("CompanyUserLastName");
        customerUser.setUserName("haba@mail.com");
        customerUser.setPhoneNumber("1234534534");
        customerUser.setReferenceId("ref-123421");

        entityManager.merge(customerUser);
    }
}
